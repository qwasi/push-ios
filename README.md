# push-ios

Example iOS project using Qwasi Push REST API

The REST API is very straight forward and only requires a few simple calls to enable a device.


## Using REST API to register your device and push token

```
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
    NSLog(@"Device Token = %@",strDevicetoken);
    
    self.strDeviceToken = strDevicetoken;
    
    // Register the device
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:@"53d3201387c6e1e017131070" forKey:@"id"];
    [params setValue:@"Rob's iPhone" forKey:@"name"];
    [params setValue:@{@"proto": @"push.apns", @"addr": strDevicetoken } forKey:@"push"];
    [params setValue:@{@"debug": [NSNumber numberWithBool: TRUE] } forKey:@"meta"];
    
    NSString *path = [NSString stringWithFormat:@"applications/%@/devices", APP_ID];
    
    [self.manager POST:path parameters:params success:^(NSURLSessionDataTask *task, NSDictionary* responseObject) {
        self.strDeviceID = responseObject[@"id"];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(error.localizedDescription);
    }];
}

```


### Fetching a message 

```
-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)remoteNotif {
    NSLog(@"handleRemoteNotification Dictionary: %@", remoteNotif);
    
    // get the dictionary of values from the notification
    NSArray* qwasi = [remoteNotif valueForKey:@"qwasi"];

    
    NSLog(@"linkURL: %@", qwasi);
    
    if (qwasi) {
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setValue:qwasi[0] forKey:@"id"];
        [params setValue:self.strDeviceID forKey:@"device"];
        
        // Get the message
        NSString *path = [NSString stringWithFormat:@"applications/%@/devices/%@/messages/%@", APP_ID, self.strDeviceID, qwasi[0]];
        
        [self.manager GET:path parameters:params success:^(NSURLSessionDataTask *task, NSDictionary* responseObject) {
            NSLog(@"%@", responseObject);
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(error.localizedDescription);
        }];
    }
}
```

### Posting events

Application events can be posted for the device using the `/events` path using the `com.qwasi.event.application.state` event type. \
Two common   `state` events are `opened` and `closed`.


```
NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
[params setValue:@"com.qwasi.event.application.state" forKey:@"type"];
[params setValue:self.strDeviceID forKey:@"device"];
[params setValue:USER_TOKEN forKey:@"user_token"];
[params setValue:@{@"state": @"opened"} forKey:@"data"];


NSString *path = [NSString stringWithFormat:@"applications/%@/devices/%@/events", APP_ID, self.strDeviceID];

[self.manager POST:path parameters:params success:^(NSURLSessionDataTask *task, NSDictionary* responseObject) {

} failure:^(NSURLSessionDataTask *task, NSError *error) {
NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];

NSLog(@"%@", serializedData[@"message"]);
}];
```
