//
//  main.m
//  AcmePush
//
//  Created by Rob Rodriguez on 8/27/19.
//  Copyright © 2019 Model Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
