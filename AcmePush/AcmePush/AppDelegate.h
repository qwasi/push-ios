//
//  AppDelegate.h
//  AcmePush
//
//  Created by Rob Rodriguez on 8/27/19.
//  Copyright © 2019 Model Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *strDeviceToken;

@property (strong, nonatomic) NSString *strDeviceID;

@property (strong, nonatomic) AFHTTPSessionManager *manager;
@end

