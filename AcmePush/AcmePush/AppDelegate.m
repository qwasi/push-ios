//
//  AppDelegate.m
//  AcmePush
//
//  Created by Rob Rodriguez on 8/27/19.
//  Copyright © 2019 Model Rocket, LLC. All rights reserved.
//

#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>


#define API_URL @"https://api.qwasi.com/rest"
#define APP_ID @"<YOUR_APP_ID>"
#define API_KEY @"<YOUR_API_KEY>"
#define USER_TOKEN @"<YOUR_USER_TOKEN>"
#define DEVICE_ID_KEY @"com.qwasi.api.device_id"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    // execute on main thread only

    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.HTTPAdditionalHeaders = @{@"Content-Type": @"application/json",
                                            @"accept": @"application/json",
                                            @"X-QWASI-API-KEY": API_KEY};
    
    
    self.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString: API_URL] sessionConfiguration:configuration];
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.responseSerializer = [AFJSONResponseSerializer serializer];

    
    // read the device id from user defaults
    self.strDeviceID = [[NSUserDefaults standardUserDefaults] objectForKey: DEVICE_ID_KEY];
    
    NSLog(@"default id %@", self.strDeviceID);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self registerForRemoteNotifications];
    });
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    if (self.strDeviceID == nil) {
        return;
    }
    
    // Register the device
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:@"com.qwasi.event.application.state" forKey:@"type"];
    
    [params setValue:USER_TOKEN forKey:@"user_token"];
    [params setValue:@{@"state": @"closed"} forKey:@"data"];
    
    if (self.strDeviceID != nil) {
        [params setValue:self.strDeviceID forKey:@"device"];
    }
    
    NSString *path = [NSString stringWithFormat:@"applications/%@/devices/%@/events", APP_ID, self.strDeviceID];
    
    [self.manager POST:path parameters:params success:^(NSURLSessionDataTask *task, NSDictionary* responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        
        NSLog(@"%@", serializedData[@"message"]);
    }];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    if (self.strDeviceID == nil) {
        return;
    }
    
    // Register the device
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:@"com.qwasi.event.application.state" forKey:@"type"];
    [params setValue:self.strDeviceID forKey:@"device"];
    [params setValue:USER_TOKEN forKey:@"user_token"];
    [params setValue:@{@"state": @"opened"} forKey:@"data"];
    
    
    NSString *path = [NSString stringWithFormat:@"applications/%@/devices/%@/events", APP_ID, self.strDeviceID];
    
    
    [self.manager POST:path parameters:params success:^(NSURLSessionDataTask *task, NSDictionary* responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        
        NSLog(@"%@", serializedData[@"message"]);
    }];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// this must be called from the main thread
- (void)registerForRemoteNotifications {
    
    NSLog(@"inside registerForRemoteNotifications");
    
    // UserNotifications are available in iOS 10 and above
    if(@available(iOS 10,*)){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                NSLog(@"registerd for remote notifications");
                
                // execute on main thread only
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
                
            }
            else
            {
                NSLog(@"error in notification registration : %@", [error localizedDescription]);
            }
        }];
    }
}

// if the user presses on the notification while the app is in background, launch the content in the passed URL in Safari
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
    
    // The method will be called on the delegate when the user responded to the notification by opening the application,
    // dismissing the notification or choosing a UNNotificationAction.
    // The delegate must be set before the application returns from applicationDidFinishLaunching:.
    
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    
    [self handleRemoteNotification:[UIApplication sharedApplication] userInfo:response.notification.request.content.userInfo];
}

// If we did successfully register for device notifications, get our device token and put it on the debug console
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
    NSLog(@"Device Token = %@", strDevicetoken);
    
    self.strDeviceToken = strDevicetoken;
    
    // Register the device
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    if (self.strDeviceID != nil) {
        [params setValue:self.strDeviceID forKey:@"device"];
    }
    [params setValue:@"Rob's iPhone" forKey:@"name"];
    [params setValue:USER_TOKEN forKey:@"user_token"];
    [params setValue:@{@"proto": @"push.apns", @"addr": strDevicetoken } forKey:@"push"];
    [params setValue:@{@"debug": [NSNumber numberWithBool: TRUE] } forKey:@"meta"];
    
    NSString *path = [NSString stringWithFormat:@"applications/%@/devices", APP_ID];
    
    NSLog(@"%@", self.manager.baseURL);
    [self.manager POST:path parameters:params success:^(NSURLSessionDataTask *task, NSDictionary* responseObject) {
        self.strDeviceID = responseObject[@"id"];
        
        NSLog(@"Device ID = %@", self.strDeviceID);
        
        // store the device id in user defaults
        [[NSUserDefaults standardUserDefaults] setObject: self.strDeviceID forKey:DEVICE_ID_KEY];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        
        NSLog(@"%@", serializedData[@"message"]);
    }];
}

// If we did not successfully register for device notifictions, log the error on the debug console
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

// Process the remote notification contents, grab the sent URL, and launch Safari with that URL
-(void) handleRemoteNotification:(UIApplication *) application userInfo:(NSDictionary *) remoteNotif {
    
    NSLog(@"handleRemoteNotification Dictionary: %@", remoteNotif);
    
    // get the dictionary of values from the notification
    NSArray* qwasi = [remoteNotif valueForKey:@"qwasi"];
    
    
    NSLog(@"linkURL: %@", qwasi);
    
    if (qwasi) {
        
    }
}

-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)remoteNotif {
    NSLog(@"handleRemoteNotification Dictionary: %@", remoteNotif);
    
    // get the dictionary of values from the notification
    NSArray* qwasi = [remoteNotif valueForKey:@"qwasi"];

    
    NSLog(@"linkURL: %@", qwasi);
    
    if (qwasi) {
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setValue:qwasi[0] forKey:@"id"];
        [params setValue:self.strDeviceID forKey:@"device"];
        
        // Get the message
        NSString *path = [NSString stringWithFormat:@"applications/%@/devices/%@/messages/%@", APP_ID, self.strDeviceID, qwasi[0]];
        
        [self.manager GET:path parameters:params success:^(NSURLSessionDataTask *task, NSDictionary* responseObject) {
            NSLog(@"%@", responseObject);
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(error.localizedDescription);
        }];
    }
}
@end
